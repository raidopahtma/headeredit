// @license MIT

#include "header_defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "header.h"

void out_of_mem();

static uint8_t *header_defs_ptr;

static uint8_t header_defs_read(uint32_t ptr){
	return(header_defs_ptr[ptr]);
}

static struct header_defs_struct *header_defs_get(uint32_t value_id);
static void *header_defs_parse_zero(char *input, int *format,  uint32_t *value_len);
static void *header_defs_parse_u8(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_u16(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_u32(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_u64(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_string(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_bin(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_usbversion(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_usbstring(char *input, int *format, uint32_t *value_len);
static void *header_defs_parse_uuid(char *input, int *format, uint32_t *value_len);
static void header_defs_print_zero(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_u8(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_u16(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_u32(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_u64(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_string(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_bin(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_usbversion(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_usbstring(FILE *stream, int format, void *value, uint32_t value_len);
static void header_defs_print_uuid(FILE *stream, int format, void *value, uint32_t value_len);

static unsigned char hex_to_uchar(char a, char b);
static size_t strtobin(unsigned char *dst, const char *src, size_t siz);

static struct header_defs_struct{
	uint32_t value_id;
	char *name;
	char *key;
	char *type;
}header_defs_list[] = {
	{
		HEADER_VALUE_ID_HEADER_OFFSET, "Header offset", "offset", "u32"
	},{
		HEADER_VALUE_ID_PLATFORM, "Platform", "platform", "u8"
	},{
		HEADER_VALUE_ID_PLATFORM16, "Platform", "platform16", "u16"
	},{
		HEADER_VALUE_ID_SOFTWARE_TYPE, "Software Type", "softtype", "u8"
	},{
		HEADER_VALUE_ID_SOFTWARE_TYPE2, "Software Type 2", "softtype2", "u8"
	},{
		HEADER_VALUE_ID_BOOTLOADER_ADDRESS, "Bootloader Address", "bootaddr", "u32"
	},{
		HEADER_VALUE_ID_BOOTLOADER_SIZE_MAX, "Bootloader Max Size", "bootsizemax", "u32"
	},{
		HEADER_VALUE_ID_FIRMWARE_ADDRESS, "Firmware Address", "firmaddr", "u32"
	},{
		HEADER_VALUE_ID_FIRMWARE_DOWNLOAD_ADDRESS, "Firmware Download Address", "firmdownaddr", "u32"
	},{
		HEADER_VALUE_ID_FIRMWARE_SIZE_MAX, "Firmware Max Size", "firmsizemax", "u32"
	},{
		HEADER_VALUE_ID_DATA_ADDRESS, "Data Address", "dataaddr", "u32"
	},{
		HEADER_VALUE_ID_DATA_SIZE_MAX, "Data Max Size", "datasizemax", "u32"
	},{
		HEADER_VALUE_ID_SIZE, "Size", "size", "u32"
	},{
		HEADER_VALUE_ID_CRC, "CRC", "crc", "u32"
	},{
		HEADER_VALUE_ID_NAME, "Name", "name", "string"
	},{
		HEADER_VALUE_ID_VERSION, "Version", "version", "string"
	},{
		HEADER_VALUE_ID_VERSION_BINARY, "Version", "versionbin", "bin"
	},{
		HEADER_VALUE_ID_USB_VENDORID, "USB Vendor ID", "usbvendid", "u16"
	},{
		HEADER_VALUE_ID_USB_PRODUCTID, "USB Product ID", "usbprodid", "u16"
	},{
		HEADER_VALUE_ID_USB_DEVICE_VERSION, "USB Device Version", "usbver", "usbver"
	},{
		HEADER_VALUE_ID_USB_MANUFACTURER, "USB Manufacturer", "usbmanu", "usbstring"
	},{
		HEADER_VALUE_ID_USB_PRODUCT, "USB Product", "usbprod", "usbstring"
	},{
		HEADER_VALUE_ID_USB_SERIAL, "USB Serial", "usbserial", "usbstring"
	},{
		HEADER_VALUE_ID_UUID, "UUID", "uuid", "uuid"
	},{
		HEADER_VALUE_ID_UUID2, "UUID2", "uuid2", "uuid"
	},{
		HEADER_VALUE_ID_UUID3, "UUID3", "uuid3", "uuid"
	},{
		HEADER_VALUE_TIMESTAMP, "Timestamp", "timestamp", "u64"
	},{
		0xFF
	}
};

static struct header_parse_struct{
	char *type;
	void *(*parse)(char *input, int *format, uint32_t *value_len);
	void (*print)(FILE *stream, int format, void *value, uint32_t value_len);
}header_parse_list[] = {
	{ "zero", header_defs_parse_zero, header_defs_print_zero },
	{ "u8", header_defs_parse_u8, header_defs_print_u8 },
	{ "u16", header_defs_parse_u16, header_defs_print_u16 },
	{ "u32", header_defs_parse_u32, header_defs_print_u32 },
	{ "u64", header_defs_parse_u64, header_defs_print_u64 },
	{ "string", header_defs_parse_string, header_defs_print_string },
	{ "bin", header_defs_parse_bin, header_defs_print_bin },
	{ "usbver", header_defs_parse_usbversion, header_defs_print_usbversion },
	{ "usbstring", header_defs_parse_usbstring, header_defs_print_usbstring },
	{ "uuid", header_defs_parse_uuid, header_defs_print_uuid },
	{ NULL }
};

void header_defs_print_ids(){
	int i, j, l, key_len = 0;
	for(i = 0; header_defs_list[i].value_id < 0xFF; i++){
		l = strlen(header_defs_list[i].key) + strlen(header_defs_list[i].type);
		if(key_len < l)key_len = l;
	}
	key_len += 2;
	for(i = 0; header_defs_list[i].value_id < 0xFF; i++){
		printf("    %s (0x%02X, %s)", header_defs_list[i].key, (unsigned int)header_defs_list[i].value_id, header_defs_list[i].type);
		l = strlen(header_defs_list[i].key) + strlen(header_defs_list[i].type);
		for(j = 0; j < (key_len - l); j++){
			printf(" ");
		}
		printf("%s\n", header_defs_list[i].name);
	}
}

void header_defs_print_types(){
	int i;
	for(i = 0; header_parse_list[i].type != NULL; i++){
		printf("    %s\n", header_parse_list[i].type);
	}
}

struct header_list *header_defs_get_by_key(char *key){
	struct header_list *header;
	uint32_t i, j;
	for(i = 0; header_defs_list[i].value_id != 0xFF; i++){
		if(!strcmp(header_defs_list[i].key, key)){
			header = malloc(sizeof(struct header_list));
			if(header == NULL)out_of_mem();
			header->value_id = header_defs_list[i].value_id;
			for(j = 0; header_parse_list[j].type != NULL; j++){
				if(!strcmp(header_defs_list[i].type, header_parse_list[j].type)){
					header->parse = header_parse_list[j].parse;
					header->print = header_parse_list[j].print;
					break;
				}
			}
			if(header_parse_list[j].type == NULL){
				header->parse = header_defs_parse_bin;
				header->print = header_defs_print_bin;
			}
			return(header);
		}
	}
	return(NULL);
}

struct header_list *header_defs_get_by_type(char *type){
	struct header_list *header;
	uint32_t i;
	for(i = 0; header_parse_list[i].type != NULL; i++){
		if(!strcmp(header_parse_list[i].type, type)){
			header = malloc(sizeof(struct header_list));
			if(header == NULL)out_of_mem();
			header->parse = header_parse_list[i].parse;
			header->print = header_parse_list[i].print;
			return(header);
		}
	}
	return(NULL);
}

static struct header_defs_struct *header_defs_get(uint32_t value_id){
	uint32_t i;
	for(i = 0; header_defs_list[i].value_id != 0xFF; i++){
		if(header_defs_list[i].value_id == value_id)return(&header_defs_list[i]);
	}
	return(NULL);
}

static void *header_defs_parse_zero(char *input, int *format, uint32_t *value_len){
	*value_len = 0;
	return(NULL);
}

static void *header_defs_parse_u8(char *input, int *format, uint32_t *value_len){
	unsigned long long int value_uint;
	uint8_t *value;
	*value_len = 1;
	if(input != NULL){
		value = malloc(*value_len);
		if(value == NULL)out_of_mem();
		value_uint = strtoull(input, NULL, 0);
		value[0] = ((value_uint >> 0) & 0xFF);
		return(value);
	}
	return(NULL);
}

static void *header_defs_parse_u16(char *input, int *format, uint32_t *value_len){
	unsigned long long int value_uint;
	uint8_t *value;
	*value_len = 2;
	if(input != NULL){
		value = malloc(*value_len);
		if(value == NULL)out_of_mem();
		value_uint = strtoull(input, NULL, 0);
		value[0] = ((value_uint >> 0) & 0xFF);
		value[1] = ((value_uint >> 8) & 0xFF);
		return(value);
	}
	return(NULL);
}

static void *header_defs_parse_u32(char *input, int *format, uint32_t *value_len){
	unsigned long long int value_uint;
	uint8_t *value;
	*value_len = 4;
	if(input != NULL){
		value = malloc(*value_len);
		if(value == NULL)out_of_mem();
		value_uint = strtoull(input, NULL, 0);
		value[0] = ((value_uint >> 0) & 0xFF);
		value[1] = ((value_uint >> 8) & 0xFF);
		value[2] = ((value_uint >> 16) & 0xFF);
		value[3] = ((value_uint >> 24) & 0xFF);
		return(value);
	}
	return(NULL);
}

static void *header_defs_parse_u64(char *input, int *format, uint32_t *value_len){
	unsigned long long int value_uint;
	uint8_t *value;
	*value_len = 8;
	if(input != NULL){
		value = malloc(*value_len);
		if(value == NULL)out_of_mem();
		value_uint = strtoull(input, NULL, 0);
		value[0] = ((value_uint >> 0) & 0xFF);
		value[1] = ((value_uint >> 8) & 0xFF);
		value[2] = ((value_uint >> 16) & 0xFF);
		value[3] = ((value_uint >> 24) & 0xFF);
		value[4] = ((value_uint >> 32) & 0xFF);
		value[5] = ((value_uint >> 40) & 0xFF);
		value[6] = ((value_uint >> 48) & 0xFF);
		value[7] = ((value_uint >> 56) & 0xFF);
		return(value);
	}
	return(NULL);
}

static void *header_defs_parse_string(char *input, int *format, uint32_t *value_len){
	char *value;
	if(input != NULL){
		*value_len = strlen(input) + 1;
		value = malloc(*value_len);
		if(value == NULL)out_of_mem();
		memcpy(value, input, *value_len - 1);
		value[*value_len - 1] = 0;
		return(value);
	}
	*value_len = 0;
	return(NULL);
}

static void *header_defs_parse_bin(char *input, int *format, uint32_t *value_len){
	unsigned char *value;
	if(input != NULL){
		value = malloc(strlen(input) / 2);
		if(value == NULL)out_of_mem();
		*value_len = strtobin(value, input, strlen(input) / 2);
		return(value);
	}
	*value_len = 0;
	return(NULL);
}

static void *header_defs_parse_usbversion(char *input, int *format, uint32_t *value_len){
	uint8_t *value, i;
	if(input != NULL){
		value = malloc(2);
		i = 0;
		value[0] = value[1] = 0;
		if(input[i] && (input[i] != '.'))value[1] = hex_to_uchar(0, input[i++]);
		if(input[i] && (input[i] != '.'))value[1] = (value[1] << 4) | hex_to_uchar(0, input[i++]);
		if(input[i] && (input[i] == '.'))i++;
		if(input[i])value[0] = (hex_to_uchar(0, input[i++]) << 4);
		if(input[i])value[0] |= hex_to_uchar(0, input[i]);
		*value_len = 2;
		return(value);
	}
	*value_len = 0;
	return(NULL);
}

static void *header_defs_parse_usbstring(char *input, int *format, uint32_t *value_len){
	uint32_t i, l;
	char *value;
	if(input != NULL){
		l = strlen(input);
		value = malloc((l * 2) + 2);
		if(value == NULL)out_of_mem();
		*value_len = value[0] = 2 + (l * 2);
		value[1] = 0x03;
		for(i = 0; i < l; i++){
			value[2 + (i * 2) + 0] = input[i];
			value[2 + (i * 2) + 1] = 0x00; // TODO: real unicode support
		}
		return(value);
	}
	*value_len = 0;
	return(NULL);
}

// 9a5c89a2-7948-41e4-a4bc-f8621e4dcb4a
static void *header_defs_parse_uuid(char *input, int *format, uint32_t *value_len){
	unsigned char *value;
	char *temp;
	int i, l;
	if(input != NULL){
		value = malloc(16);
		if(value == NULL)out_of_mem();
		temp = malloc(33);
		if(temp == NULL)out_of_mem();
		for(i = 0, l = 0; i < strlen(input) && (l < 32); i++){
			if(isxdigit(input[i])){
				temp[l++] = input[i];
			}
		}
		temp[l] = 0;
		*value_len = strtobin(value, temp, l);
		free(temp);
		return(value);
	}
	*value_len = 0;
	return(NULL);
}

static void header_defs_print_zero(FILE *stream, int format, void *value, uint32_t value_len){
}

static void header_defs_print_u8(FILE *stream, int format, void *value, uint32_t value_len){
	header_defs_ptr = value;
	fprintf(stream, "0x%02X", (unsigned int)header_u8(0, header_defs_read));
}

static void header_defs_print_u16(FILE *stream, int format, void *value, uint32_t value_len){
	header_defs_ptr = value;
	fprintf(stream, "0x%04X", (unsigned int)header_u16(0, header_defs_read));
}

static void header_defs_print_u32(FILE *stream, int format, void *value, uint32_t value_len){
	header_defs_ptr = value;
	fprintf(stream, "0x%08X", (unsigned int)header_u32(0, header_defs_read));
}

static void header_defs_print_u64(FILE *stream, int format, void *value, uint32_t value_len){
	header_defs_ptr = value;
	fprintf(stream, "0x%016llX", (unsigned long long int)header_u64(0, header_defs_read));
}

static void header_defs_print_string(FILE *stream, int format, void *value, uint32_t value_len){
	fprintf(stream, "%s", (char *)value);
}

static void header_defs_print_bin(FILE *stream, int format, void *value, uint32_t value_len){
	uint32_t i;
	for(i = 0; i < value_len; i++){
		if(i)fprintf(stream, " ");
		fprintf(stream, "%02X", (unsigned int)((uint8_t *)value)[i]);
	}
}

static void header_defs_print_usbversion(FILE *stream, int format, void *value, uint32_t value_len){
	fprintf(stream, "%x.%02x", (unsigned int)((uint8_t *)value)[1], (unsigned int)((uint8_t *)value)[0]);
}

static void header_defs_print_usbstring(FILE *stream, int format, void *value, uint32_t value_len){
	uint32_t i;
	for(i = 2; i < value_len; i += 2){
		fprintf(stream, "%c", (int)((char *)value)[i]);
	}
}

// 9a5c89a2-7948-41e4-a4bc-f8621e4dcb4a
static void header_defs_print_uuid(FILE *stream, int format, void *value, uint32_t value_len){
	uint32_t i;
	if(value_len != 16){
		fprintf(stream, "(error)");
		return;
	}
	for(i = 0; i < value_len; i++){
		if((i == 4) || (i == 6) || (i == 8) || (i == 10))fprintf(stream, "-");
		fprintf(stream, "%02x", (unsigned int)((uint8_t *)value)[i]);
	}
}

void header_defs_print_name(FILE *stream, uint32_t value_id){
	struct header_defs_struct *header_defs;
	header_defs = header_defs_get(value_id);
	if(header_defs != NULL){
		fprintf(stream, "%s", header_defs->name);
	}else{
		fprintf(stream, "Value ID %u", (unsigned int)value_id);
	}
}

void header_defs_print_value(FILE *stream, int format, uint32_t value_id, void *value, uint32_t value_len){
	struct header_defs_struct *header_defs;
	int j;
	header_defs = header_defs_get(value_id);
	if(header_defs != NULL){
		for(j = 0; header_parse_list[j].type != NULL; j++){
			if(!strcmp(header_defs->type, header_parse_list[j].type)){
				header_parse_list[j].print(stream, format, value, value_len);
				return;
			}
		}
	}
	header_defs_print_bin(stream, format, value, value_len);
}

static size_t strtobin(unsigned char *dst, const char *src, size_t siz){
	size_t i, t;
	t = (siz << 1);
	for(i = 0; i < t; i += 2){
		if(!src[i] || !src[i + 1])break;
		if(isxdigit(src[i]) && isxdigit(src[i + 1])){
			if(dst != NULL)dst[i >> 1] = hex_to_uchar(src[i], src[i + 1]);
		}else break;
	}
	return(i >> 1);
}

static unsigned char hex_to_uchar(char a, char b){
	unsigned char t;
	t = 0;
	if(a == '1')t = 16;
	if(a == '2')t = 32;
	if(a == '3')t = 48;
	if(a == '4')t = 64;
	if(a == '5')t = 80;
	if(a == '6')t = 96;
	if(a == '7')t = 112;
	if(a == '8')t = 128;
	if(a == '9')t = 144;
	if(a == 'A' || a == 'a')t = 160;
	if(a == 'B' || a == 'b')t = 176;
	if(a == 'C' || a == 'c')t = 192;
	if(a == 'D' || a == 'd')t = 208;
	if(a == 'E' || a == 'e')t = 224;
	if(a == 'F' || a == 'f')t = 240;
	if(b == '1')t |= 1;
	if(b == '2')t |= 2;
	if(b == '3')t |= 3;
	if(b == '4')t |= 4;
	if(b == '5')t |= 5;
	if(b == '6')t |= 6;
	if(b == '7')t |= 7;
	if(b == '8')t |= 8;
	if(b == '9')t |= 9;
	if(b == 'A' || b == 'a')t |= 10;
	if(b == 'B' || b == 'b')t |= 11;
	if(b == 'C' || b == 'c')t |= 12;
	if(b == 'D' || b == 'd')t |= 13;
	if(b == 'E' || b == 'e')t |= 14;
	if(b == 'F' || b == 'f')t |= 15;
	return(t);
}

