// @license MIT

#ifndef _HEADER_DEFS_H_
#define _HEADER_DEFS_H

#include <stdio.h>
#include <stdint.h>

struct header_list{
	uint32_t value_id;
	void *(*parse)(char *input, int *format, uint32_t *value_len);
	void (*print)(FILE *stream, int format, void *value, uint32_t value_len);
	int format;
	uint32_t value_len;
	uint8_t *new_value;
	uint8_t *old_value;
	struct header_list *next;
};

struct header_list *header_defs_get_by_key(char *key);
struct header_list *header_defs_get_by_type(char *type);
void header_defs_print_name(FILE *stream, uint32_t value_id);
void header_defs_print_value(FILE *stream, int format, uint32_t value_id, void *value, uint32_t value_len);

void header_defs_print_ids();
void header_defs_print_types();

#endif

