LIBDIR = lib

all: linux-native

linux-native:
	gcc -Wall -I$(LIBDIR) $(LIBDIR)/header.c $(LIBDIR)/header_crc.c $(LIBDIR)/crc32.c header_defs.c file.c main.c -o headeredit

win32:
	i686-w64-mingw32-gcc -DWINDOWS -Wall -I$(LIBDIR) $(LIBDIR)/header.c $(LIBDIR)/header_crc.c $(LIBDIR)/crc32.c header_defs.c file.c main.c -o headeredit-w32.exe

win64:
	x86_64-w64-mingw32-gcc -DWINDOWS -Wall -I$(LIBDIR) $(LIBDIR)/header.c $(LIBDIR)/header_crc.c $(LIBDIR)/crc32.c header_defs.c file.c main.c -o headeredit-w64.exe

install:
	cp headeredit /usr/bin

uninstall:
	rm -rf /usr/bin/headeredit

clean:
	rm -rf headeredit headeredit-w32.exe headeredit-w64.exe test.bin

test:
	./headeredit -c -v 3,u16,0x1234 -v 6,u32,0x00040000 -v 7,u32,0x000A0000 -v 8,u32,0x00060000 -v 9,u32,0 -v 10,u32,0 -v "12,string,kala2" test.bin

test2:
	./headeredit -v 3,u16,0x1234 test.bin

