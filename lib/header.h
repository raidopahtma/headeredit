// @license MIT

#ifndef _HEADER_H_
#define _HEADER_H_

#include <stdint.h>

#define HEADER_TOKEN_LEN                          21
#define HEADER_MIN_LEN                            (HEADER_TOKEN_LEN + 1)

#define HEADER_VALUE_ID_HEADER_OFFSET             1  // uint32
#define HEADER_VALUE_ID_PLATFORM                  2  // uint8
#define HEADER_VALUE_ID_PLATFORM16                3  // uint16
#define HEADER_VALUE_ID_SOFTWARE_TYPE             4  // uint8  0 - bootloader, 1 - firmware, 2 - test
#define HEADER_VALUE_ID_SOFTWARE_TYPE2            5  // uint8
#define HEADER_VALUE_ID_BOOTLOADER_ADDRESS        6  // uint32
#define HEADER_VALUE_ID_BOOTLOADER_SIZE_MAX       7  // uint32
#define HEADER_VALUE_ID_FIRMWARE_ADDRESS          8  // uint32
#define HEADER_VALUE_ID_FIRMWARE_SIZE_MAX         9  // uint32
#define HEADER_VALUE_ID_DATA_ADDRESS              10 // uint32
#define HEADER_VALUE_ID_DATA_SIZE_MAX             11 // uint32
#define HEADER_VALUE_ID_FIRMWARE_DOWNLOAD_ADDRESS 12 // uint32
#define HEADER_VALUE_ID_SIZE                      13 // uint32
#define HEADER_VALUE_ID_CRC                       14 // uint32
#define HEADER_VALUE_ID_NAME                      15 // string
#define HEADER_VALUE_ID_VERSION                   16 // string
#define HEADER_VALUE_ID_VERSION_BINARY            17 // bin
#define HEADER_VALUE_ID_IEEE_ADDRESS              18 // bin8
#define HEADER_VALUE_ID_DEFAULT_IP                19 // bin4
#define HEADER_VALUE_ID_USB_VENDORID              20 // u16
#define HEADER_VALUE_ID_USB_PRODUCTID             21 // u16
#define HEADER_VALUE_ID_USB_DEVICE_VERSION        22 // u16
#define HEADER_VALUE_ID_USB_MANUFACTURER          23 // usb string
#define HEADER_VALUE_ID_USB_PRODUCT               24 // usb string
#define HEADER_VALUE_ID_USB_SERIAL                25 // usb string
#define HEADER_VALUE_ID_UUID                      26 // bin16
#define HEADER_VALUE_ID_UUID2                     27 // bin16
#define HEADER_VALUE_ID_UUID3                     28 // bin16
#define HEADER_VALUE_TIMESTAMP                    29 // uint64

uint8_t header_read(uint32_t ptr);

uint8_t header_u8(uint32_t ptr, uint8_t (*read)(uint32_t ptr));
uint16_t header_u16(uint32_t ptr, uint8_t (*read)(uint32_t ptr));
uint32_t header_u32(uint32_t ptr, uint8_t (*read)(uint32_t ptr));
uint64_t header_u64(uint32_t ptr, uint8_t (*read)(uint32_t ptr));

uint32_t header_find(uint32_t start, uint32_t end, uint8_t (*read)(uint32_t ptr));

uint32_t header_get(uint32_t header, uint32_t value_id, uint32_t minlen, uint32_t *len, uint8_t (*read)(uint32_t ptr));
uint8_t header_get_u8(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr));
uint16_t header_get_u16(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr));
uint32_t header_get_u32(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr));
uint32_t header_get_cstr(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr));

uint32_t header_calc_crc(uint32_t start, uint32_t end, uint32_t exclude, uint8_t (*read)(uint32_t ptr));
uint8_t header_crc_ok(uint32_t start, uint32_t end, uint32_t header, uint8_t (*read)(uint32_t ptr));

#endif

