// @license MIT

#ifndef _FILE_H_
#define _FILE_H_

#include <stdint.h>

int file_open(char *filename, uint32_t *len);
uint8_t file_read(uint32_t ptr);
void *file_mem(uint32_t ptr);
int file_close();

int file_create(char *filename);
int file_write(const void *buf, int count);
void file_xor(uint8_t xor_value);

#endif

