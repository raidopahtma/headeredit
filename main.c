// @license MIT

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include "file.h"
#include "header.h"
#include "header_defs.h"
#include "crc32.h"

static char *color_reset = "";
static char *color_red = "";
static char *color_yellow = "";
static char *color_green = "";

static int header_result(char *filename, int created, struct header_list *header_list);
static int header_edit(char *filename, struct header_list *header_list);
static int header_create(char *filename, struct header_list *header_list);
static int header_list_add(struct header_list **header_list, struct header_list *header_list_next, char *input);
static struct header_list *header_list_find(struct header_list *header_list, uint32_t value_id);

void out_of_mem(){
	fprintf(stderr, "%sError: out of memory%s\n", color_red, color_reset);
	exit(1);
}

int main(int argc, char *argv[]){
	FILE *f;
	int i, c, create, blind, xor;
	char *value_type, *input, *filename, *array;
	uint32_t value_id, len;
	uint8_t xor_value, a;
	struct header_list *header_list, *header_list_next;
	crc32_init();
	header_list = NULL;
	create = blind = xor = 0;
	array = NULL;
	opterr = 0;
	while((c = getopt(argc, argv, "hbCcx:a:v:")) != -1){
		if(c == 'h'){
			printf("Header editor, Rebane 2016, rebane@alkohol.ee\n");
			printf("Usage: %s [options] file\n", argv[0]);
			printf("  Options:\n");
			printf("    -h                                   show this help\n");
			printf("    -b                                   blind output\n");
			printf("    -C                                   color output\n");
			printf("    -c                                   create header file\n");
			printf("    -x <value>                           XOR each byte in file with value\n");
			printf("    -a <filename>                        create comma separated list of bytes\n");
			printf("    -v [value id,[value type,[value]]]   create/modify header entry\n");
			printf("  Value id's:\n");
			header_defs_print_ids();
			printf("  Value types:\n");
			header_defs_print_types();
			return(0);
		}else if(c == 'c'){
			create = 1;
		}else if(c == 'b'){
			blind = 1;
		}else if(c == 'C'){
			color_reset = "\e[0m";
			color_red = "\e[31m";
			color_yellow = "\e[1m\e[33m";
			color_green = "\e[32m";
		}else if(c == 'x'){
			xor_value = strtoull(optarg, NULL, 0);
			xor = 1;
		}else if(c == 'a'){
			array = optarg;
		}else if(c == 'v'){
			header_list_next = NULL;
			value_type = input = NULL;
			value_id = 0;
			for(i = 0; ; i++){
				c = ((char *)optarg)[i];
				if((c == ',') || (c == 0)){
					((char *)optarg)[i] = 0;
					if(value_type == NULL){
						header_list_next = header_defs_get_by_key(optarg);
						if(header_list_next == NULL){
							value_id = strtoull(optarg, NULL, 0);
							if(c != 0)value_type = &((char *)optarg)[++i];
						}else{
							if(header_list_next->value_id == HEADER_VALUE_ID_HEADER_OFFSET){
								value_type = "u32";
							}else if(header_list_next->value_id == HEADER_VALUE_ID_SIZE){
								value_type = "u32";
							}else if(header_list_next->value_id == HEADER_VALUE_ID_CRC){
								value_type = "u32";
							}else{
								value_type = "";
							}
							if(c != 0)input = &((char *)optarg)[++i];
							break;
						}
					}else{
						if(c != 0)input = &((char *)optarg)[++i];
						break;
					}
					if(c == 0)break;
				}
			}
			if(header_list_next == NULL){
				header_list_next = header_defs_get_by_type(value_type);
				if(header_list_next == NULL){
					fprintf(stderr, "%sError: invalid value type%s\n", color_red, color_reset);
					return(1);
				}
				if((value_id == 0) || (value_id > 0xFE)){
					fprintf(stderr, "%sError: invalid value ID%s\n", color_red, color_reset);
					return(1);
				}
				header_list_next->value_id = value_id;
			}
			if(input == NULL){
				if(header_list_next->value_id == HEADER_VALUE_ID_HEADER_OFFSET){
					if(strcmp(value_type, "u32")){
						fprintf(stderr, "%sError: invalid value type for autodetection%s\n", color_red, color_reset);
						return(1);
					}
				}else if(header_list_next->value_id == HEADER_VALUE_ID_SIZE){
					if(strcmp(value_type, "u32")){
						fprintf(stderr, "%sError: invalid value type for autodetection%s\n", color_red, color_reset);
						return(1);
					}
				}else if(header_list_next->value_id == HEADER_VALUE_ID_CRC){
					if(strcmp(value_type, "u32")){
						fprintf(stderr, "%sError: invalid value type for autodetection%s\n", color_red, color_reset);
						return(1);
					}
				}else{
					fprintf(stderr, "%sError: value cannot be autodetected%s\n", color_red, color_reset);
					return(1);
				}
			}
			if(header_list_add(&header_list, header_list_next, input) != 0)return(1);
		}else{
			fprintf(stderr, "%sError: invalid command line%s\n", color_red, color_reset);
			return(1);
		}
	}
	filename = argv[optind];
	if(filename == NULL){
		fprintf(stderr, "%sError: missing file name%s\n", color_red, color_reset);
		return(1);
	}
	if(xor){
		if(file_open(filename, &len) < 0){
			fprintf(stderr, "%sError: open: %s%s\n", color_red, strerror(errno), color_reset);
			return(1);
		}
		if(!blind)printf("File length: 0x%08X (%u)\n", (unsigned int)len, (unsigned int)len);
		file_xor(xor_value);
		if(!blind)printf("XOR'ed with value: 0x%02X\n", (unsigned int)xor_value);
		if(file_close() < 0){
			fprintf(stderr, "%sError: close: %s%s\n", color_red, strerror(errno), color_reset);
			return(1);
		}
	}else if(array != NULL){
		if(file_open(filename, &len) < 0){
			fprintf(stderr, "%sError: open: %s%s\n", color_red, strerror(errno), color_reset);
			return(1);
		}
		if(!blind){
			if(!strcmp(array, "-")){
				fprintf(stderr, "File length: 0x%08X (%u)\n", (unsigned int)len, (unsigned int)len);
			}else{
				printf("File length: 0x%08X (%u)\n", (unsigned int)len, (unsigned int)len);
			}
		}
		if(!strcmp(array, "-")){
			f = stdout;
		}else{
			f = fopen(array, "w");
			if(f == NULL){
				fprintf(stderr, "%sError: open: %s%s\n", color_red, strerror(errno), color_reset);
				return(1);
			}
		}
		for(i = 0; i < len; i++){
			a = file_read(i);
			if(!(i % 8)){
				fprintf(f, "\t");
			}else{
				fprintf(f, " ");
			}
			if((i + 1) == len){
				fprintf(f, "0x%02X", (unsigned int)a);
#ifdef WINDOWS
				fprintf(f, "\r\n");
#else
				fprintf(f, "\n");
#endif
			}else{
				fprintf(f, "0x%02X,", (unsigned int)a);
				if((i % 8) == 7){
#ifdef WINDOWS
				fprintf(f, "\r\n");
#else
				fprintf(f, "\n");
#endif
				}
			}
		}
		if(strcmp(array, "-"))fclose(f);
		if(!blind){
			if(!strcmp(array, "-")){
				fprintf(stderr, "Created byte array\n");
			}else{
				printf("Created byte array\n");
			}
		}
		if(file_close() < 0){
			fprintf(stderr, "%sError: close: %s%s\n", color_red, strerror(errno), color_reset);
			return(1);
		}
	}else{
		if(create && (header_create(filename, header_list) != 0))return(1);
		if(header_edit(filename, header_list) != 0)return(1);
		if(!blind && header_result(filename, create, header_list) != 0)return(1);
	}
	return(0);
}

static int header_result(char *filename, int created, struct header_list *header_list){
	int retval, n;
	uint32_t len, value_id, value_len, header, value, next_header;
	struct header_list *header_next;
	if(file_open(filename, &len) < 0){
		fprintf(stderr, "%sError: open: %s%s\n", color_red, strerror(errno), color_reset);
		return(1);
	}
	if(len < HEADER_MIN_LEN){
		fprintf(stderr, "%sError: file too small%s\n", color_red, color_reset);
		file_close();
		return(1);
	}
	retval = 1;
	printf("File length: 0x%08X (%u)\n", (unsigned int)len, (unsigned int)len);
	header = header_find(0, len, file_read);
	if(header == 0){
		fprintf(stderr, "%sError: header not found%s\n", color_red, color_reset);
		goto ready;
	}
	n = 1;
	do{
		next_header = ((header - HEADER_TOKEN_LEN) + 1);
		printf("Found header offset: 0x%08X (%u)\n", (unsigned int)(header - HEADER_TOKEN_LEN), (unsigned int)(header - HEADER_TOKEN_LEN));
		printf("Calculated CRC: 0x%08X\n", (unsigned int)header_calc_crc(0, len, header_get(header, HEADER_VALUE_ID_CRC, 4, NULL, file_read), file_read));
		printf("Embedded values:\n");
		for( ; file_read(header) != 0xFF; header += file_read(header + 1) + 2){
			value_id = file_read(header);
			value = header_get(header, value_id, 0, &value_len, file_read);
			header_next = header_list_find(header_list, value_id);
			if(!created && (header_next != NULL) && (header_next->old_value != NULL) && value_len){
				printf("  ");
				printf("%s", color_yellow);
				header_defs_print_name(stdout, value_id);
				printf(": ");
				header_next->print(stdout, header_next->format, header_next->old_value, value_len);
				printf("%s - set: ", color_green);
				header_next->print(stdout, header_next->format, file_mem(value), value_len);
				printf("%s\n", color_reset);
			}else{
				printf("  ");
				header_defs_print_name(stdout, value_id);
				if(value_len){
					printf(": ");
					header_defs_print_value(stdout, 0, value_id, file_mem(value), value_len);
				}
				printf("\n");
			}
		}
		header = header_find(next_header, len, file_read);
		if(header)printf("\nHeader #%u:\n", (unsigned int)++n);
	}while(header);
	retval = 0;
ready:
	if(file_close() < 0){
		fprintf(stderr, "%sError: close: %s%s\n", color_red, strerror(errno), color_reset);
		return(1);
	}
	return(retval);
}

static int header_edit(char *filename, struct header_list *header_list){
	int retval;
	uint32_t len, value_len, value_uint, header, value;
	struct header_list *header_next;
	if(file_open(filename, &len) < 0){
		fprintf(stderr, "%sError: open: %s%s\n", color_red, strerror(errno), color_reset);
		return(1);
	}
	if(len < HEADER_MIN_LEN){
		fprintf(stderr, "%sError: file too small%s\n", color_red, color_reset);
		file_close();
		return(1);
	}
	retval = 1;
	header = header_find(0, len, file_read);
	if(header == 0){
		fprintf(stderr, "%sError: header not found%s\n", color_red, color_reset);
		goto ready;
	}
	if(header_find((header - HEADER_TOKEN_LEN) + 1, (len - ((header - HEADER_TOKEN_LEN) + 1)), file_read)){
		fprintf(stderr, "%sError: found more than one header%s\n", color_red, color_reset);
		goto ready;
	}
	for(header_next = header_list; header_next != NULL; header_next = header_next->next){
		value = header_get(header, header_next->value_id, 0, &value_len, file_read);
		if(value == 0){
			fprintf(stderr, "%sError: value ID not found: %u%s\n", color_red, (unsigned int)header_next->value_id, color_reset);
			goto ready;
		}else if(value_len != header_next->value_len){
			fprintf(stderr, "%sError: value size mismatch: %u, %u != %u%s\n", color_red, (unsigned int)header_next->value_id, (unsigned int)value_len, (unsigned int)header_next->value_len, color_reset);
			goto ready;
		}
		if(header_next->new_value == NULL){
			if(header_next->value_id == HEADER_VALUE_ID_HEADER_OFFSET){
				header_next->new_value = malloc(4);
				if(header_next->new_value == NULL){
					fprintf(stderr, "%sError: out of memory%s\n", color_red, color_reset);
					goto ready;
				}
				value_uint = (header - HEADER_TOKEN_LEN);
				header_next->new_value[0] = ((value_uint >> 0) & 0xFF);
				header_next->new_value[1] = ((value_uint >> 8) & 0xFF);
				header_next->new_value[2] = ((value_uint >> 16) & 0xFF);
				header_next->new_value[3] = ((value_uint >> 24) & 0xFF);
			}else if(header_next->value_id == HEADER_VALUE_ID_SIZE){
				header_next->new_value = malloc(4);
				if(header_next->new_value == NULL){
					fprintf(stderr, "%sError: out of memory%s\n", color_red, color_reset);
					goto ready;
				}
				value_uint = len;
				header_next->new_value[0] = ((value_uint >> 0) & 0xFF);
				header_next->new_value[1] = ((value_uint >> 8) & 0xFF);
				header_next->new_value[2] = ((value_uint >> 16) & 0xFF);
				header_next->new_value[3] = ((value_uint >> 24) & 0xFF);
			}
		}
		if(header_next->new_value == NULL)continue;
		if(memcmp(file_mem(value), header_next->new_value, value_len)){
			header_next->old_value = malloc(value_len);
			if(header_next->old_value == NULL){
				fprintf(stderr, "%sError: out of memory%s\n", color_red, color_reset);
				goto ready;
			}
			memcpy(header_next->old_value, file_mem(value), value_len);
			memcpy(file_mem(value), header_next->new_value, value_len);
		}else{
			header_next->old_value = NULL;
		}
	}
	for(header_next = header_list; header_next != NULL; header_next = header_next->next){
		if((header_next->value_id != HEADER_VALUE_ID_CRC) || (header_next->new_value != NULL))continue;
		value = header_get(header, header_next->value_id, 0, &value_len, file_read);
		header_next->new_value = malloc(4);
		if(header_next->new_value == NULL){
			fprintf(stderr, "%sError: out of memory%s\n", color_red, color_reset);
			goto ready;
		}
		value_uint = header_calc_crc(0, len, value, file_read);
		header_next->new_value[0] = ((value_uint >> 0) & 0xFF);
		header_next->new_value[1] = ((value_uint >> 8) & 0xFF);
		header_next->new_value[2] = ((value_uint >> 16) & 0xFF);
		header_next->new_value[3] = ((value_uint >> 24) & 0xFF);
		if(memcmp(file_mem(value), header_next->new_value, value_len)){
			header_next->old_value = malloc(value_len);
			if(header_next->old_value == NULL){
				fprintf(stderr, "%sError: out of memory%s\n", color_red, color_reset);
				goto ready;
			}
			memcpy(header_next->old_value, file_mem(value), value_len);
			memcpy(file_mem(value), header_next->new_value, value_len);
		}else{
			header_next->old_value = NULL;
		}
	}
	retval = 0;
ready:
	if(file_close() < 0){
		fprintf(stderr, "%sError: close: %s%s\n", color_red, strerror(errno), color_reset);
		return(1);
	}
	return(retval);
}

static int header_create(char *filename, struct header_list *header_list){
	int i, retval;
	uint8_t t;
	if(file_create(filename) < 0){
		fprintf(stderr, "%sError: open: %s%s\n", color_red, strerror(errno), color_reset);
		return(1);
	}
	retval = 1;
	if(file_write("*INFORMATION  HEADER*", 21) != 21){
		fprintf(stderr, "%sError: write: %s%s\n", color_red, strerror(errno), color_reset);
		goto ready;
	}
	for( ; header_list != NULL; header_list = header_list->next){
		t = header_list->value_id;
		if(file_write(&t, 1) != 1){
			fprintf(stderr, "%sError: write: %s%s\n", color_red, strerror(errno), color_reset);
			goto ready;
		}
		t = header_list->value_len;
		if(file_write(&t, 1) != 1){
			fprintf(stderr, "%sError: write: %s%s\n", color_red, strerror(errno), color_reset);
			goto ready;
		}
		if(header_list->value_len){
			if(header_list->new_value == NULL){
				for(i = 0; i < header_list->value_len; i++){
					if(file_write("", 1) != 1){
						fprintf(stderr, "%sError: write: %s%s\n", color_red, strerror(errno), color_reset);
						goto ready;
					}
				}
			}else{
				if(file_write(header_list->new_value, header_list->value_len) != header_list->value_len){
					fprintf(stderr, "%sError: write: %s%s\n", color_red, strerror(errno), color_reset);
					goto ready;
				}
			}
		}
	}
	t = 0xFF;
	if(file_write(&t, 1) != 1){
		fprintf(stderr, "%sError: write: %s%s\n", color_red, strerror(errno), color_reset);
		goto ready;
	}
	retval = 0;
ready:
	if(file_close() < 0){
		fprintf(stderr, "%sError: close: %s%s\n", color_red, strerror(errno), color_reset);
		return(1);
	}
	return(retval);
}

static int header_list_add(struct header_list **header_list, struct header_list *header_list_next, char *input){
	if(header_list_next->value_id == 0){
		fprintf(stderr, "%sError: invalid value ID%s\n", color_red, color_reset);
		return(1);
	}
	header_list_next->old_value = NULL;
	header_list_next->new_value = header_list_next->parse(input, &header_list_next->format, &header_list_next->value_len);
	for( ; *header_list != NULL; header_list = &((*header_list)->next));
	*header_list = header_list_next;
	(*header_list)->next = NULL;
	return(0);
}

static struct header_list *header_list_find(struct header_list *header_list, uint32_t value_id){
	for( ; header_list != NULL; header_list = header_list->next){
		if(header_list->value_id == value_id)return(header_list);
	}
	return(NULL);
}

